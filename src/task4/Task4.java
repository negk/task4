
package task4;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.Random;
import java.util.HashMap;
        

public class Task4 {
    public static Scanner datos = new Scanner(System.in);
    
    
    public static void main(String[] args) {
        int opc;
        
        do {
            System.out.println("\n------- Menu -------\n1- Par e Impar con Listas\n2- Par e Impar con Diccionarios\n3- Generar matriz de temperaturas\n4- Salir");
            opc = datos.nextInt();
            
            switch (opc) {
                case 1 -> ejercicio1();
                case 2 -> ejercicio2();
                case 3 -> ejercicio3();
                case 4 -> {
                    System.out.println("Saliendo...");
                    datos.close();
                }
                default -> System.out.println("Opcion invalida, intente de nuevo.");
            }
        
        } while (opc != 4);
        
        
    }
    
    
    public static void ejercicio1() {
          System.out.println("");
//        int[] numerosArreglo = {}; , int[] signosArreglo = {}; Este tipo de arreglo no es el mismo que el siguiente puesto que no se puede modifcar el tamaño durante la ejecucion, cosa que ArrayList<> si es mas flexible para ello
          ArrayList<Integer> numerosRandomArreglo = new ArrayList<>(); // Un arreglo dinamico que permite agregar o eliminar elementos en momento de la ejecucion, el anterior son estaticas hasta que acabe la ejecucion
          ArrayList<Character> signosArreglo = new ArrayList<>(); // Character pq solo guardaremos un unico dato

          // Agregar numeros a la lista
          Random rand = new Random();
          for (int i = 0; i < 10; i++) { // capacidad 10 numeros con un random de 0 a 100
            numerosRandomArreglo.add(rand.nextInt(100) + 1); // siguiente random entero ( 0 a 100 ) 
          }
          
          // Intercambiar los pares e impares con sus signos respectivos
          for (int num : numerosRandomArreglo) { // num es iteracion de numerosArreglo
              if (num % 2 == 0) {
                  signosArreglo.add('$'); // Cada numero par se convierte en dolar
              } else {
                  signosArreglo.add('#');
              }
          }
          
          System.out.println(numerosRandomArreglo + "\n" + signosArreglo);
    }
    
    
    public static void ejercicio2() {
        System.out.println("");
        
        ArrayList<Integer> numerosAzar = new ArrayList<>();
        Random random = new Random();
        for (int i = 0; i < 10; i++) {
            numerosAzar.add(random.nextInt(100) + 1);
        }
        
        HashMap<Integer, Character> signos = new HashMap<>(); // Ver metodo set para uso con for
        for (int num : numerosAzar) {
            if (num % 2 == 0) {
                signos.put(num, '$');
            } else {
                signos.put(num, '#');
            }
        }
        System.out.println(numerosAzar + "\n" + signos);
    }
    
    
    public static void ejercicio3() {
        System.out.println();
        int[][] matrizTemperaturas = new int[4][7];
        int totalSemanas = 4;
        int totalDias = 7;
        int temperatura;
        
        Random random = new Random();
        for (int semana = 0; semana < totalSemanas; semana++) {
            for (int dia = 0; dia < totalDias; dia++) {
                matrizTemperaturas[semana][dia] = random.nextInt(32) + 7;
            }
        }
        
        System.out.println("Temperaturas de Febrero\nL  K  M  J  V  S  D");
        for (int semana = 0; semana < totalSemanas; semana++) {
            for (int dia = 0; dia < totalDias; dia++) {
                temperatura = matrizTemperaturas[semana][dia]; // actualizamos valor para validaciones
                
                if (temperatura < 15) {                                         
                    System.out.print("\u001B[34m" + temperatura + " \u001B[0m");// Temperatura baja
                } else if (temperatura > 15 & temperatura < 25) {               
                    System.out.print("\u001B[33m" + temperatura + " \u001B[0m");// Temperatura media
                } else{                                                         
                    System.out.print("\u001B[31m" + temperatura + " \u001B[0m");// Temperatura alta
                }
                 
            }
            System.out.println("Semana" + (semana + 1));
        }
        int opc;
        System.out.println("");
        do {
            System.out.println("\nVer mas resultados\n1- Temperaturas mas altas y bajas de cada semana\n2- Mostrar el promedio de temperatura de cada una de las semanas\n3- Mostrar la temperatura mas alta de todo el mes\n4- Volver al menu principal");
            opc = datos.nextInt();

            switch (opc) {
                case 1 -> tempOpcion1(matrizTemperaturas, totalSemanas, totalDias);
                case 2 -> tempOpcion2(matrizTemperaturas, totalSemanas, totalDias);
                case 3 -> tempOpcion3(matrizTemperaturas, totalSemanas, totalDias);
                case 4 -> System.out.println("Volviendo al menu... ");
                default -> System.out.println("Opcion invalida, intente de nuevo.");
            }

        } while (opc != 4);
    }
    public static void tempOpcion1(int[][] matrizTemperaturas, int totalSemanas, int totalDias) {
        String[] diasString = {"Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado", "Domingo"};
        int minTemp = 38;
        int maxTemp = 7;
        int diaminTemp = 0;
        int diamaxTemp = 0;
        System.out.println();
        
        for (int semana = 0; semana < totalSemanas; semana++) {
            for (int dia = 0; dia < totalDias; dia++) {
                int temperatura = matrizTemperaturas[semana][dia];
                
                if (minTemp > temperatura) {
                    minTemp = temperatura;
                    diaminTemp = dia;
                }
                if (maxTemp < temperatura) {
                    maxTemp = temperatura;
                    diamaxTemp = dia;
                }
            }
            
            System.out.printf("La semana %d tuvo el dia mas caluroso el %s con %d grados y su dia mas fresco el %s con %d grados\n" , (semana + 1), diasString[diamaxTemp], maxTemp, diasString[diaminTemp], minTemp);
            minTemp = 38;
            maxTemp = 7;
            diaminTemp = 0;
            diamaxTemp = 0;
        }
    }
    
    public static void tempOpcion2(int[][] matrizTemperaturas, int totalSemanas, int totalDias) {
        for (int semana = 0; semana < totalSemanas; semana++) {
            for (int dia = 0; dia < totalDias; dia++) {
                
            }
        }
    }
    
    public static void tempOpcion3(int[][] matrizTemperaturas, int totalSemanas, int totalDias) {
        for (int semana = 0; semana < totalSemanas; semana++) {
            for (int dia = 0; dia < totalDias; dia++) {
                
            }
        }
    }
    
}
